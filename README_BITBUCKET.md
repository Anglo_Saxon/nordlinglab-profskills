# How to get write access to the NordlingLab-ProfSkills GIT repository?

1. Sign up to participate in the course and get access to the material by registering in [this Google Sheet](https://docs.google.com/spreadsheets/d/1FJ47smHB1JHGm_l5yGB0FVr8EQMDhTEKzvfjo4xibE0/edit?usp=sharing). The purpose of this list is to enable you to use your existing account(s) and to see who is actually following the course.

1. Wait until you get an invitation email from "Atlassian <noreply@am.atlassian.com>" with subject "temn invited you to collaborate on the NordlingLab-ProfSkills repository on Bitbucket" and click *Accept my invitation*. (Check your spam folder before emailing the Professor or TA.)![](https://bitbucket.org/temn/nordlinglab-introai/downloads/Email_invitation.png)

1. Either sign up for a new Atlassian/Bitbucket account by filling in your full name (in English please), password, and clicking *Sign up*, or *Log in* if you already have an Atlassian account.![](https://bitbucket.org/temn/nordlinglab-introai/downloads/Bitbucket_sign_up_screen.png)

1. When you see the "You have been invited to a repository" screen, click *Accept invitation*.![](https://bitbucket.org/temn/nordlinglab-introai/downloads/Bitbucket_invitation_screen.png)

