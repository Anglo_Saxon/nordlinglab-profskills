## 2019-09-25 2nd class (2019-09-19)

1.The Ted Talk vedio gave me a deep inspiration. I always thinks that everything in the past is beautiful until I watched this video.
2.Other team's presentations let me know there are so many commodity prices fall as the number grows exponentially I had never realize before.

## 2019-09-26 3rd class

1.Today's video main topic is about fake news. Compared with the war, the terrorist organization, the unemployment rate,
  the fake news is closer to our lives.
2.Coincidentally, our prensentation is related to the literacy rate. 
  I'll try to find out whether the relationship between the improvement of literacy rate and the ability to distinguish fake news.  

## 2019-10-03

1.Today we have three videos. All of them focused on economy. 
2.I used to think that banks are just a place to save and borrow money. I don't know the great influence of the bank until today.

## 2019-10-17

1.Today's video is about extremism, such as facism.
2.If people always feel lonely, they are likely to participate in ultra-nationalist.

## 2019-10-24

1.Today's video is about our health, especially our brain.
2.I think today's video is very close to our lives.
3.We often ignore physical and mental connections and cause everything bad because of one of them has problem.

## 2019-10-31

1.Today's video is about depression, which is often ignored.
2.More and more people commit suicide to solve problems, but that is the worst choice.

## 2019-11-07

1.Today's video is about happiness, which is everyone longs for.
2.I think the reason for happiness comes from the process and not the result.

## 2019-11-14

1.Today's video is about law in UK.
2.With the differences between the laws of the United Kingdom and Taiwan, I understand that with the differences in regions, countries, and beliefs, 
  the differences between people will be great, and the most important thing is mutual respect.
  
## 2019-11-21

1.Today's video is about axiety caused by smartphone.
2.Although the Net play a more and more important role today, sometimes talk face to face is more efficient.
3.We should be careful not to indulge in smart phones.

## 2019-11-28

1.Today's video is about history always repeats itself,like USA and Rome.
2.Money is much more important than I thought, stable economic structure brings stable society.

## 2019-12-05

1.Today's video is about nine planetary boundaries, some of them are common, and some of them I have never heard before.
2.We should reduce the damage to the environment.

## 2019-12-12

1.Today's topic is about planetary boundary.
2.Our planet faces many problems that we rarely know.
3.Many of the debates in the classroom also made me know more about our planet

## 2019-12-19

1.Today's debate is about the third industrial revolution.
2.We watched a video on how to learn without motivation and it helped me a lot.

## 2019-12-26

1.Today we spend all our time discussing how to be more successful and productive.
2.I read two articles about AI, studied deep learning and listen to Ted talks about future world.
3.I will get up earlier and do my best to  follow the schedule strictly.

## 2020-01-02

1.Today was the last day of class.

##Diary

20191226
1.Successful and Unproductive
2.I prepare for final exam of engineering mathematics well.
3.I will arrange my time for introduction to materials science report.

20191227
1.Productive
2.I think i can get good grade in engineering mathematics, i finish my introduction to materials science report.
3.I will prepare for my final exams and reports.

20191228
1.Successful and Productive
2.I have to design a teaching aid or a table game related to discrete mathematics.
3.I will prepare for my final exams and reports.

20191229
1.Successful 
2.Study for final exam.
3.I will prepare for my final exams and reports.

20191230
1.Unroductive
2.Finish discrete mathematics report I think I can do better.
3.I will prepare for my final exams and reports.

20191231
1.Unseccessful
2.Participate in New Year's Eve events, did not study at all.
3.I will prepare for my final exams and reports.

20200101
1.Successful 
2.Study for final exam.
3.I will prepare for my final exams .