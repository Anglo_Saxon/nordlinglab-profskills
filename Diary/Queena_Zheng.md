This diary file is written by Queena Zheng E14074120 in the course Professional skills for engineering the third industrial revolution.

## 2020-09-24 3nd class

* The course is very useful and can learn many presentation skills.
* Our topic is about homicide and this issue should be valued.

## 2020-10-07 4th class

* On today's lecture our group made a lot of mistakes on our presetation.
* I thought we didn't get the point that  professor wanted us to notice.
* The points like blank space, last visited date, no reference...etc.
* Today's homework is about financial, so the report needs more time to find the sources.
* The loading of this course may be bigger than I think.

## 2020-10-14 5th class
 * Today's couse is about financial which we don't have much knowing.
 * The presatation of our group still got mistakes about blank space.
 * Today's lecture is a little bit boring because the topic we didn't familiar with.
 * The video's topic is about fascim which was totally opposite to Taiwan's policy.
 * Hope next week's topic may have much fun.
 
## 2020-10-21 6th class
 
 * Today's prentation is myth.But our group misunderstand the meaning of myth in English and Chinese.
 * The experiment about pressure test is really useful for reducing anxiety.
 * Next topic is about health, and it's a important issue that should be valued.
 * The main way to get a health body is having regular exerise behavior.
 * Nowasays, mentel problems have been confirmed that will effect your health. 
 
## 2020-10-29 7th class
 
 * Today's presentation is about healthy life, and it's an important issue we should valued.
 * I think sleep is the main factor to effect our health.
 * Though regular sleep time is impossible in colloge life, taking a nap can help us to focus on the course.
 * Next topic is about depression, and I think most people are still afraid of talking about it in Taiwan.
 * About the final project I still don't know what we are going to do, and I help professor can talk more about it.
 
## 2020-11-04 8th class
 * Today's presentation is about depression, and in modern society many people seem suffer with it.
 * I think the best way to help depressed people is accompanying them and listening their thoughts.
 * Functional depression is caracterized by the inability to "just get over it".
 * People should be patient to anyone who might be with depression.
 * In my weekend, I slept before 1 a.m., and my body seemed healthier than in school day.
 * Though stay up late is not healthy to our bodies, I thought we couldn't prevent it.
 
## 2020-11-12 9th class
 * Today's presentation is about our big group project, and this topic is about current credit and monetary.
 * This topic is a tough issue for us, because we are engineers.
 * The modern system is not the best for our society, but we don't really figure out a good way to fix or change it.
 * The next presentation is about legal law, and I didn't have gone to a court.

## 2020-11-19 10th class
 * Today I go to Taoyuan to participate in the billiards competition so I apply an occupational leave.
 
## 2020-11-26 11th class
 * Today I get a fever, so I don't go to the course on time and don't get my rollcall.
 * I am too tired to get my attention on today's lecture.
 *  About the final project I still don't know what we are going to do, and I help professor can talk more about it.

## 2020-12-03 12th class
 * Today I get a fever, so I don't go to the course on time and don't get my rollcall.
 * I am too tired to get my attention on today's lecture.
 *  About the final project I still don't know what we are going to do, and I help professor can talk more about it.

## 2020-12-10 13th class

 * Today's lecture is about the nine planetary boundaries and our group topic is biosphere integrity.
 * We think that the ways to preserve the biosphere integrity are do not overdevelope the rainforest save marine resources and don't overuse of pesticides.
 * Habitat loss and degradation due to farming especially animal agriculture.
 * Climate change through heat stress and drought stress
 
## 2020-12-17 14th class
 * 201218 FRI.
 * A. Unsuccessful and unproductive
 * B. I felt unsuccessful because I was late for school. I felt unproductive because we didn't really figured out for the term project.
 * C. I will get up tomorrow at 10:00 instead of 14:00 like last Saturday.  
  * 201219 SAT.
 * A. Successful and unproductive
 * B. I woke up early this morning, but didn't do lots of things. 
 * C. I will get up tomorrow at 10:00 instead of 14:00 like last Saturday.  
  * 201220 SUN.
 * A. Unsuccessful and unproductive
 * B. I felt unsuccessful because I woke up late this morning.
 * C. I felt unproductive because I just did one homework and didn't do other thing.
  * 201221 MON.
 * A. Unsuccessful and unproductive
 * B. I felt unsuccessful because I was late for school.
 * C. Today was too cold to do other thing, so I wasted my time.
  * 201222 TUE.
 * A. Successful and productive
 * B. I felt successful because I did my term project done.
 * C. I did my work done.
  * 201223 WED.
 * A. Unsuccessful and unproductive
 * B. I spent too much time on watching video, and didn't do other thing.
 * C. I will get up tomorrow at 10:00 instead of 14:00 like last Saturday.  
 
## 2020-12-24 15th class
 * 201225 FRI.
 * A. Successful and productive
 * B. I felt successful because we did our final project of control system done. I felt productive because we did it perfectly.
 * C. I will study my final exam for at least 2 hours.  
  * 201226 SAT.
 * A. Successful and unproductive
 * B. I felt successful becauseI woke up early this morning, and I felt unproductive because I didn't do much thing. 
 * C. I will get up tomorrow at 10:00 instead of 14:00 like last Saturday.  
  * 201227 SUN.
 * A. Successful and productive
 * B. I felt successful because we did our final presentation of fluid dynamics done. I felt productive because we did it on time.
 * C. I will review the mechanism desigh for final quiz.
  * 201228 MON.
 * A. Successful and unproductive
 * B. I felt successful because I got good grades on mechanism desigh. 
 * C. I felt unproductive because the project of control system was error and we didn't figure it out. .
  * 201229 TUE.
 * A. unsuccessful and unproductive
 * B. I felt unsuccessful because I was late for school.
 * C. I felt unproductive because the project of control system was still error and we didn't figure it out.
  * 201230 WED.
 * A. Unsuccessful and productive
 * B. I felt unsuccessful because tomorrow is the final test of control system, and I am afraid of failing the test.
 * C. I felt produtive because my project of wave energy is almost done.  
 
## 2020-12-31 16th class
  * Today is the last day of 2020, and the lecture of today that every group had to present 3 idea of their topic which can help others to know their solution.
    But we didn't do well on this presentation, I hoped that we can do better on the video.
    Next time is the lastest course of this semester, and I think the class is interesting in helping us to know more about the presentation in English, giving us a chance to present.
    